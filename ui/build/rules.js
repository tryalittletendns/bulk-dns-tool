import path from 'path';

import config from './config';

export default function() {
    return [
        {
            test   : /\.ts$/,
            loaders: [
                {
                    loader : 'awesome-typescript-loader',
                    options: {
                        configFileName: path.join(config.root, 'tsconfig.json')
                    }
                },
                'angular2-template-loader'
            ]
        },
        {
            test: /\.html$/,
            use : [
                {
                    loader : 'html-loader',
                    options: {
                        minimize: false
                    }
                }
            ]
        },
        {
            test   : /\.s?css$/,
            loaders: ['raw-loader', 'sass-loader']
        },
        {
            test  : /\.(png|jpg|jpeg|svg|gif|eot|ttf|woff2?)$/,
            loader: 'url-loader?limit=1024'
        }
    ];
}
