import path from 'path';

const root = path.resolve(__dirname, '..');

export default {
    root,
    app : path.join(root, 'app'),
    dist: path.join(root, 'dist')
}
