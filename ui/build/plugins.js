import path from 'path';
import webpack from 'webpack';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import {CheckerPlugin} from 'awesome-typescript-loader';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import configJson from '../config.json';

import config from './config';

export default function(prod) {
    const htmlPluginConfig = {
        template: path.join(config.app, 'index.ejs'),
        inject  : true
    };

    if (prod) {
        htmlPluginConfig.minify = {
            caseSensitive       : true,
            collapseWhitespace  : true,
            conservativeCollapse: true
        };
    }

    return [
        new CleanWebpackPlugin([config.dist], {
            root: config.root
        }),
        new CheckerPlugin(),
        new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)@angular/,
            config.app,
            {} // a map of your routes
        ),
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),
        new HtmlWebpackPlugin(htmlPluginConfig),
        new FaviconsWebpackPlugin(path.resolve(config.app, 'assets', 'images', 'favicon.png')),
        new webpack.DefinePlugin({
            'process.env': JSON.stringify(createEnv())
        })
    ];
};

function createEnv() {
    const env = configJson;

    if (!env.SERVER_HOST) {
        env.SERVER_HOST = 'localhost';
    }

    if (!env.SERVER_PORT) {
        env.SERVER_PORT = 3000;
    }

    if (!env.SERVER_PROTOCOL) {
        env.SERVER_PROTOCOL = 'http';
    }

    return env;
}
