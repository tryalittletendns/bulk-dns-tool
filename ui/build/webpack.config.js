import path from 'path';

import config from './config';
import rules from './rules';
import plugins from './plugins';

export default function(env) {
    const prod = env === 'prod';

    return {
        context      : config.root,
        entry        : {
            app      : path.join(config.app, 'index.ts'),
            polyfills: path.join(config.app, 'polyfills.ts'),
            vendor   : path.join(config.app, 'vendor.ts')
        },
        output       : {
            path    : config.dist,
            filename: 'bulk-dns-tool.[name].[hash].js'
        },
        resolve      : {
            extensions: ['.ts', '.js', '.json']
        },
        resolveLoader: {
            alias: {
                'splash-loader': path.join(config.root, 'build', 'SplashIndexLoader')
            }
        },
        module       : {
            rules: rules(prod)
        },
        plugins      : plugins(prod),
        devtool      : prod ? 'source-map' : 'inline-source-map'
    };

}
