import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import MainModule from './bulk-dns/bulk-dns.module';

import '!style-loader!css-loader!sass-loader!./index.scss';
import {enableProdMode} from '@angular/core';

if (process.env.NODE_ENV === 'production') {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(MainModule);

if (module.hot) {
    module.hot.accept();
}
