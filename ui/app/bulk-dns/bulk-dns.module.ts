import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {Ng2CompleterModule} from 'ng2-completer';

import AppComponent from './components/app/app.component';
import NavbarComponent from './components/navbar/navbar.component';
import CommonHeaderComponent from './components/common-header/common-header.component';
import ModalWindowComponent from './components/modal-window/modal-window.component';
import routes from './bulk-dns.routes';
import ApiService from './services/api.service';
import {WindowRef} from './services/window-ref.service';
import SpinnerComponent from './components/spinner/spinner.component';
import CommonHeaderButtonComponent from './components/common-header/common-header-button.component';
import {IconSelectorModalComponent} from './components/icon-selector-modal/icon-selector-modal.component';
import StyleService from './services/style.service';
import LoginPageComponent from './components/login-page/login-page.component';
import FeedbackButtonComponent from './components/feedback-button/feedback-button.component';
import PocListComponent from './components/poc-list/poc-list.component';
import UserPickerComponent from './components/user-picker/user-picker.component';
import CustomCompleterService from './services/customCompleter.service';
import {CustomRemoteDataFactoryProvider} from './services/completerData.factory';
import HomePageComponent from './components/home-page/home-page.component';
import NavWrapperPageComponent from './components/nav-wrapper-page/nav-wrapper-page.component';
import ResultsPageComponent from './components/results-page/results-page.component';
import BatchListPageComponent from './components/batch-list-page/batch-list-page.component';
import BatchListComponent from './components/batch-list/batch-list.component';
import StatusLozengeComponent from './components/status-lozenge/status-lozenge.component';
import ResultsTableComponent from './components/results-table/results-table.component';
import BatchInfoComponent from './components/batch-info/batch-info.component';
import WhoisModalComponent from './components/whois-modal/whois-modal.component';
import GeoModalComponent from './components/geo-modal/geo-modal.component';
import ExtendExpiryModalComponent from './components/extend-expiry-modal/extend-expiry-modal.component';

@NgModule({
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule.forRoot(routes),
        Ng2CompleterModule
    ],
    declarations: [
        AppComponent,
        NavbarComponent,
        NavWrapperPageComponent,
        CommonHeaderComponent,
        CommonHeaderButtonComponent,
        ModalWindowComponent,
        SpinnerComponent,
        IconSelectorModalComponent,
        LoginPageComponent,
        FeedbackButtonComponent,
        PocListComponent,
        UserPickerComponent,
        HomePageComponent,
        ResultsPageComponent,
        BatchListPageComponent,
        BatchListComponent,
        StatusLozengeComponent,
        ResultsTableComponent,
        BatchInfoComponent,
        WhoisModalComponent,
        GeoModalComponent,
        ExtendExpiryModalComponent
    ],
    providers   : [
        ApiService,
        StyleService,
        WindowRef,
        CustomCompleterService,
        CustomRemoteDataFactoryProvider
    ],
    bootstrap   : [AppComponent]
})
export default class MainModule {
}
