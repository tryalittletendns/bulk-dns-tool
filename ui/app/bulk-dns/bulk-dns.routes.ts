import NavWrapperPageComponent from './components/nav-wrapper-page/nav-wrapper-page.component';
import LoginPageComponent from './components/login-page/login-page.component';
import HomePageComponent from './components/home-page/home-page.component';
import ResultsPageComponent from './components/results-page/results-page.component';
import BatchListPageComponent from './components/batch-list-page/batch-list-page.component';

export default [
    {
        path     : 'login',
        component: LoginPageComponent
    },
    {
        path      : '',
        redirectTo: 'home',
        pathMatch : 'full'
    },
    {
        path     : '',
        component: NavWrapperPageComponent,
        children : [
            {
                path     : 'home',
                component: HomePageComponent
            },
            {
                path     : 'batches',
                component: BatchListPageComponent
            },
            {
                path     : 'batches/:id',
                component: ResultsPageComponent
            }
        ]
    },
    {
        path      : '**',
        redirectTo: 'home'
    }
];
