const packageJson = require('../../../package.json');

export default {
    appName          : 'Bulk DNS Lookup Tool',
    service          : {
        host    : process.env.SERVER_HOST,
        port    : process.env.SERVER_PORT,
        protocol: process.env.SERVER_PROTOCOL,
        get url(): string {
            return `${this.protocol}://${this.host}:${this.port}/api`;
        }
    },
    auth             : {
        basic: true
    },
    version          : packageJson.version,
    markdownGuideUrl : 'https://simplemde.com/markdown-guide',
    /* tslint:disable-next-line:max-line-length */
    issueCollectorUrl: '',
    google           : {
        apiKey: process.env.GOOGLE_API_KEY
    }
};
