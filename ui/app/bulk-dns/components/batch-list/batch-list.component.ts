import {Component, Input} from '@angular/core';
import Batch from '../../models/Batch';

@Component({
    selector   : 'batch-list',
    templateUrl: './batch-list.template.html',
    styleUrls  : ['./batch-list.styles.scss']
})
export default class BatchListComponent {
    @Input() private batches: Batch[] = [];
}
