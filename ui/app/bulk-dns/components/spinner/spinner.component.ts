import {Component, Input} from '@angular/core';

@Component({
    selector   : 'spinner',
    templateUrl: './spinner.template.html',
    styleUrls  : ['./spinner.styles.scss']
})
export default class SpinnerComponent {
    @Input('className') private className: string;
}
