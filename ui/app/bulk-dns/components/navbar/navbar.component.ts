import {Component, Input} from '@angular/core';
import appConfig from '../../config/appConfig';
import {NavigationEnd, Router} from '@angular/router';
import StyleService from '../../services/style.service';
import Style from '../../models/Style';
import NavTab from '../../models/NavTab';

const iconUrl = require('../../../assets/images/favicon.svg');

@Component({
    selector   : 'navbar',
    templateUrl: './navbar.template.html',
    styleUrls  : ['./navbar.styles.scss']
})
export default class ProblemListComponent {
    private title = appConfig.appName;
    private styleClass: string;
    private activeTab: NavTab;
    private iconUrl = iconUrl;

    private tabs: NavTab[] = [
        {title: 'Home', path: '/home', style: Style.PRIMARY},
        {title: 'Batches', path: '/batches', style: Style.SECONDARY}
    ];

    @Input() private user: object;

    constructor(private router: Router, private styleService: StyleService) {
        router.events.subscribe(e => {
            if (e instanceof NavigationEnd) {
                const activeRoute = this.getBaseRoute(e.urlAfterRedirects);

                this.activeTab = this.findTab(activeRoute);
                this.setStyle(this.activeTab ? this.activeTab.style : Style.PRIMARY);
            }
        });
    }

    private setStyle(style: Style): void {
        this.styleClass = this.styleService.getStyleClass(style);
    }

    private getBaseRoute(url: string): string {
        const urlParts = url.split('/').filter(Boolean);
        return urlParts.length ? urlParts[0] : '';
    }

    private findTab(route: string): NavTab {
        return this.tabs.find(tab => {
            return this.getBaseRoute(tab.path) === route;
        });
    }
}
