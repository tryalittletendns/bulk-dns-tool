import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector   : 'common-header',
    templateUrl: './common-header.template.html',
    styleUrls  : ['./common-header.styles.scss']
})
export default class CommonHeaderComponent {
    @Input() private heading: string;

    @Input() private iconClass: string;

    @Input() private allowAdd: boolean;

    @Input() private addTooltip: string;

    @Input() private className: string;

    @Output() private add = new EventEmitter<void>();

    @Output() private help = new EventEmitter<void>();

    private onAdd(): void {
        this.add.emit();
    }

    private onHelp(): void {
        this.help.emit();
    }
}
