import {Component, Input} from '@angular/core';

@Component({
    selector   : 'common-header-button',
    templateUrl: './common-header-button.template.html',
    styleUrls  : ['./common-header-button.styles.scss']
})
export default class CommonHeaderButtonComponent {
    @Input() private iconClass: string;
    @Input() private title: string;
}
