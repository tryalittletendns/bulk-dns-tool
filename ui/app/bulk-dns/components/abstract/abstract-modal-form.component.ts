import AbstractFormComponent from './abstract-form-component';
import {EventEmitter, Output} from '@angular/core';

export default class AbstractModalFormComponent extends AbstractFormComponent {
    @Output() protected cancel = new EventEmitter<void>();

    protected onCancel(): void {
        this.cancel.emit();
    }
}
