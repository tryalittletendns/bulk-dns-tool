import * as uuid from 'uuid';

export default class AbstractFormComponent {
    protected id = uuid.v1();

    protected getElementId(id: string): string {
        return `${id}-${this.id}`;
    }
}
