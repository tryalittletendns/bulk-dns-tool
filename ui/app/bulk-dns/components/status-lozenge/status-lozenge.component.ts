import {Component, Input} from '@angular/core';

@Component({
    selector   : 'status-lozenge',
    templateUrl: './status-lozenge.template.html',
    styleUrls  : ['./status-lozenge.styles.scss']
})
export default class StatusLozengeComponent {
    @Input() private status: string;

    private getStatus(): string {
        return this.status;
    }

    private getStatusClass(): string {
        return this.getStatus().toLowerCase().replace(/\s+/g, '-');
    }
}
