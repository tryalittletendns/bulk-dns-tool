import {Component, OnInit} from '@angular/core';
import appConfig from '../../config/appConfig';
import ApiService from '../../services/api.service';

@Component({
    selector   : 'nav-wrapper',
    templateUrl: './nav-wrapper-page.template.html',
    styleUrls  : ['./nav-wrapper-page.styles.scss']
})
export default class NavWrapperPageComponent implements OnInit {
    private user: object;
    private appName = appConfig.appName;
    private version = appConfig.version;

    constructor(private api: ApiService) {
    }

    public ngOnInit(): void {
        this.api.getCurrentUser().subscribe(
            user => this.user = user,
            err => console.error(err)
        );
    }
}
