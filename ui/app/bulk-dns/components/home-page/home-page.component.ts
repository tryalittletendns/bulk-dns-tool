import {Component, ElementRef, ViewChild} from '@angular/core';
import appConfig from '../../config/appConfig';
import ApiService from '../../services/api.service';
import {Router} from '@angular/router';
import AbstractFormComponent from '../abstract/abstract-form-component';
import * as Papa from 'papaparse';
import * as classNames from 'classnames';

const ipRegex  = /\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/,
      urlRegex = /^(https?:\/\/)*[a-z0-9-]+(\.[a-z0-9-]+)+(\/[a-z0-9-]+)*\/?$/;

@Component({
    templateUrl: './home-page.template.html',
    styleUrls  : ['./home-page.styles.scss']
})
export default class HomePageComponent extends AbstractFormComponent {
    private appName = appConfig.appName;
    private submitPending = false;
    private dragDropSupported = false;
    private isDragged = false;
    private labelText: string;
    private uploading = false;
    private manualEntry = false;
    private stripHeader = false;

    @ViewChild('sourceDataInput') private sourceDataInput: ElementRef;

    constructor(private api: ApiService, private router: Router) {
        super();
        this.checkDragDropSupport();
    }

    private checkDragDropSupport(): void {
        const div = document.createElement('div');
        if ('draggable' in div || ('ondragstart' in div && 'ondrop' in div)) {
            this.dragDropSupported = true;
            this.labelText = 'Pick a CSV to upload, or drag it here.';
        } else {
            this.labelText = 'Pick a CSV to upload.';
        }
    }

    private onFormSubmit(): void {
        if (!this.validateForm()) {
            return;
        }

        const searchTerms = this.getTextArea().value.split('\n')
            .filter(searchTerm => searchTerm);
        this.api.createBatch(searchTerms)
            .subscribe(batch => {
                this.router.navigate(['batches', batch._id]);
            });
    }

    private getTextArea(): HTMLTextAreaElement {
        return this.sourceDataInput.nativeElement as HTMLTextAreaElement;
    }

    private validateForm(): boolean {
        return !!this.getTextArea().value;
    }

    private onUploadDragEnter(e: DragEvent): void {
        this.preventDefaults(e);
        this.isDragged = true;
    }

    private onUploadDragLeave(e: DragEvent): void {
        this.preventDefaults(e);
        this.isDragged = false;
    }

    private onUploadDrop(e: DragEvent): void {
        this.preventDefaults(e);
        this.isDragged = false;
        this.uploading = true;
        this.handleFileUpload(e.dataTransfer.files);
    }

    private preventDefaults(e: Event): void {
        e.preventDefault();
        e.stopPropagation();
    }

    private onUploadInputChange(e: Event): void {
        this.uploading = true;
        this.handleFileUpload((e.target as HTMLInputElement).files);
    }

    private handleFileUpload(files: FileList): void {
        if (files.length) {
            this.parseFile(files[0])
                .then(rows => {
                    const searchTerms = rows
                        .map(row => row.length ? row[0] : '')
                        .filter(term => term);
                    this.submitData(searchTerms);
                });
        }
    }

    private toggleManual(): void {
        this.manualEntry = !this.manualEntry;
    }

    private getInstructionText(): string {
        return this.manualEntry
            ? 'Enter a list of IP addresses and URLs to query, one per line'
            : 'Upload a file containing IP addresses and URLs to query, one per line';
    }

    private parseFile(file: File): Promise<string[]> {
        return new Promise(resolve => {
            Papa.parse(file, {
                complete: results => {
                    if (this.stripHeader) {
                        results.data.shift();
                    }

                    resolve(results.data);
                }
            });
        });
    }

    private extractValidSearchTerms(rows: string[]): string[] {
        return rows.filter(row => {
            if (!row) {
                return false;
            }

            if (row !== '0.0.0.0' && row !== '255.255.255.255' && ipRegex.test(row.toString())) {
                return true;
            }

            return urlRegex.test(row);
        });
    }

    private submitData(rows: string[]): void {
        const searchTerms = this.extractValidSearchTerms(rows);
        this.api.createBatch(searchTerms)
            .subscribe(batch => {
                this.router.navigate(['batches', batch._id]);
            });
    }

    private getUploadFormClass(): string {
        return classNames('file-upload-form', {
            'drag-drop': this.dragDropSupported,
            'dragged'  : this.isDragged,
            'uploading': this.uploading
        });
    }

}
