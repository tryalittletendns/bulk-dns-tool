import {Component, Input} from '@angular/core';
import Batch from '../../models/Batch';
import * as moment from 'moment';

const searchTermLimit = 5;

@Component({
    selector   : 'batch-info',
    templateUrl: './batch-info.template.html',
    styleUrls  : ['./batch-info.styles.scss']
})
export default class BatchInfoComponent {
    @Input() private batch: Batch;

    private getDateStarted(): string {
        return this.batch ? moment(this.batch.dateStarted).fromNow() : '';
    }

    private getDateCompleted(): string {
        if (!this.batch || !this.batch.dateComplete) {
            return 'Pending';
        }

        return moment(this.batch.dateComplete).fromNow();
    }

    private getSampleTerms(): string[] {
        return this.batch.searchItems.filter((searchTerm, i) => i < 5);
    }

    private hasMoreTerms(): boolean {
        return this.batch.searchItems.length > searchTermLimit;
    }

    private getMoreText(): string {
        return `${this.batch.searchItems.length - searchTermLimit} more...`;
    }

    private getBatchStatus(): string {
        return this.batch.complete ? 'Complete' : 'In Progress';
    }

    private getTtl(): string {
        return this.batch ? moment(this.batch.ttl).fromNow() : '';
    }
}
