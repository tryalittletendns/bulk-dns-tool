import {Component, EventEmitter, Input, Output, ViewEncapsulation} from '@angular/core';
import User from '../../models/User';
import {CompleterItem, RemoteData} from 'ng2-completer';
import ApiService from '../../services/api.service';
import appConfig from '../../config/appConfig';
import CustomCompleterService from '../../services/customCompleter.service';

const userIcon = require('!file-loader!../../../assets/images/user.svg');

@Component({
    selector     : 'user-picker',
    templateUrl  : './user-picker.template.html',
    styleUrls    : ['./user-picker.styles.scss'],
    encapsulation: ViewEncapsulation.None
})
export default class UserPickerComponent {
    private search: string;
    private dataService: RemoteData;

    @Input() private inputId: string;
    @Output() private selected = new EventEmitter<User>();

    constructor(private completerService: CustomCompleterService, private api: ApiService) {
        const searchUrl = `${appConfig.service.url}/users/search`;
        this.dataService = completerService.remote(searchUrl, 'name,email', 'name')
            .urlFormater(term => `${searchUrl}?search=${encodeURIComponent(term)}`)
            .itemDecorator(item => {
                if (item && item.originalObject) {
                    const user      = item.originalObject,
                          emailText = user.email ? ` (${user.email})` : '';
                    item.title = `${user.name}${emailText}`;

                    if (user._id) {
                        item.image = userIcon;
                    }
                }

                return item;
            })
            .imageField(userIcon)
            .requestOptions(api.getDefaultRequestOptions());
    }

    public clear(): void {
        // setTimeout fixes ExpressionChangedAfterItHasBeenCheckedError caused by improper event ordering in the
        // ng2-completer component
        setTimeout(() => this.search = '');
    }

    private onSelected(item: CompleterItem): void {
        if (item) {
            this.selected.emit(item.originalObject);
        }
    }
}
