import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import BatchResult from '../../models/BatchResult';
import AbstractModalFormComponent from '../abstract/abstract-modal-form.component';
import appConfig from '../../config/appConfig';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import GeoIpData from '../../models/GeoIpData';

@Component({
    selector   : 'geo-modal',
    templateUrl: './geo-modal.template.html',
    styleUrls  : ['./geo-modal.styles.scss']
})
export default class GeoModalComponent extends AbstractModalFormComponent implements OnChanges {
    @Input() private result: BatchResult;

    private apiKey = appConfig.google.apiKey;
    private mapUrl: SafeResourceUrl;

    constructor(private sanitizer: DomSanitizer) {
        super();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.result && changes.result.currentValue) {
            this.setMapUrl(changes.result.currentValue.geoip);
        }
    }

    private setMapUrl(geoIp: GeoIpData): void {
        let params = '';

        if (!geoIp) {
            return;
        }

        if (geoIp.ll) {
            const lat = geoIp.ll[0],
                  lon = geoIp.ll[1];
            // params = `q=${lat},${lon}`;
            params = `q=${lat},${lon}&center=${lat},${lon}&zoom=11`;
        } else if (geoIp.city) {
            params = `q=${geoIp.city}`;
        } else if (geoIp.country) {
            params = `q=${geoIp.country}`;
        }

        this.mapUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
            `https://www.google.com/maps/embed/v1/place?key=${this.apiKey}&${params}`);
    }
}
