import {Component} from '@angular/core';
import ApiService from '../../services/api.service';
import Batch from '../../models/Batch';

@Component({
    templateUrl: './batch-list-page.template.html',
    styleUrls  : ['./batch-list-page.styles.scss']
})
export default class BatchListPageComponent {
    private batches: Batch[];
    private loading = false;

    constructor(private api: ApiService) {
        setTimeout(() => {
            this.loading = true;
            this.api.getBatches()
                .subscribe(batches => {
                    this.batches = batches || [];
                    this.loading = false;
                });
        });
    }
}
