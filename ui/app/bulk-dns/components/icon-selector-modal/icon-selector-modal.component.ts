import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import FontAwesomeIcon from '../../models/FontAwesomeIcon';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import ApiService from '../../services/api.service';
import StyleService from '../../services/style.service';
import AbstractModalFormComponent from '../abstract/abstract-modal-form.component';

const data                                                     = require('font-awesome-icons/data/icons.json'),
      icons                                                    = data.icons as FontAwesomeIcon[],
      filterIndex: { [filter: string]: FontAwesomeIcon[] }     = {},
      categoryIndex: { [category: string]: FontAwesomeIcon[] } = {};

icons.forEach(icon => {
    icon.categories.forEach(category => {
        const iconList = (categoryIndex[category] || (categoryIndex[category] = []));
        iconList.push(icon);
    });

    if (icon.filter) {
        icon.filter.forEach(filter => {
            let iconList: FontAwesomeIcon[];
            if (filterIndex.hasOwnProperty(filter)) {
                iconList = filterIndex[filter];
            } else {
                iconList = (filterIndex[filter] = []);
            }
            iconList.push(icon);
        });
    }
});

export interface IconChangeEvent {
    icon: string;
    iconColour: string;
}

@Component({
    selector   : 'icon-selector-modal',
    templateUrl: './icon-selector-modal.template.html',
    styleUrls  : ['./icon-selector-modal.styles.scss']
})
export class IconSelectorModalComponent extends AbstractModalFormComponent implements OnInit, OnChanges {
    private categories = ['All Icons'].concat(Object.keys(categoryIndex));
    private selectedCategory = this.categories[0];
    private filteredIcons = icons;
    private selectedIcon: FontAwesomeIcon;
    private selectedColour: string;
    private searchControl = new FormControl();
    private palette: string[];

    @Input() private currentIcon: string;
    @Input() private currentColour: string;

    @Output() private iconChange = new EventEmitter<IconChangeEvent>();

    constructor(private api: ApiService, private styleService: StyleService) {
        super();
        this.palette = styleService.getVizColours()
            .concat([
                '#999',
                '#515151',
                '#000'
            ]);
    }

    public ngOnInit(): void {
        this.searchControl.valueChanges
            .debounceTime(200)
            .subscribe(newValue => this.onSearch(newValue));
    }

    public ngOnChanges(changes: SimpleChanges): void {
        this.initSelectedIcon(changes);
        this.initSelectedIconColour(changes);
    }

    private initSelectedIcon(changes: SimpleChanges): void {
        let id: string;

        if (changes.currentIcon && changes.currentIcon.currentValue) {
            id = changes.currentIcon.currentValue;
        } else if (changes.model && changes.model.currentValue) {
            id = changes.model.currentValue.icon;
        }

        id = id || 'question';
        if (id.startsWith('fa-')) {
            id = id.substring(3);
        }

        this.selectedIcon = this.findIcon(id) || this.findIcon('question');
        this.currentIcon = `fa-${this.selectedIcon.id}`;
    }

    private initSelectedIconColour(changes: SimpleChanges): void {
        let colour = '';

        if (changes.currentColour && changes.currentColour.currentValue) {
            colour = changes.currentColour.currentValue;
        } else if (changes.model && changes.model.currentValue) {
            colour = changes.model.currentValue.iconColour;
        }

        this.currentColour = this.selectedColour = colour;
    }

    private getCurrentIcon(): string {
        return this.currentIcon || 'fa-question';
    }

    private onIconChange(): void {
        const icon       = `fa-${this.selectedIcon.id}`,
              iconColour = this.selectedColour;
        this.persistChange(icon, iconColour)
            .subscribe(
                () => this.iconChange.emit({icon, iconColour}),
                err => console.error(err)
            );
    }

    private persistChange(icon: string, iconColour: string): Observable<any> {
        return Observable.empty();
    }

    private updateModel(): Observable<any> {
        return Observable.empty();
    }

    private onSelectCategory(category: string): void {
        this.selectedCategory = category;
        this.searchControl.setValue('');

        if (category === 'All Icons') {
            this.filteredIcons = icons;
        } else {
            this.filteredIcons = categoryIndex[category] || [];
        }
    }

    private onSelectIcon(icon: FontAwesomeIcon): void {
        this.selectedIcon = icon;
    }

    private onSearch(value: string): void {
        if (!value) {
            this.onSelectCategory(this.selectedCategory || 'All Icons');
            return;
        }

        this.selectedCategory = '';
        value = value.toLowerCase();

        this.filteredIcons = icons.filter(icon => {
            if (icon.id.toLowerCase().indexOf(value) >= 0) {
                return true;
            }

            if (icon.name.toLowerCase().indexOf(value) >= 0) {
                return true;
            }

            if (icon.filter) {
                return icon.filter.some(filter => {
                    return filter.toLowerCase().indexOf(value) >= 0;
                });
            }

            return false;
        });
    }

    private findIcon(id: string): FontAwesomeIcon {
        let result: FontAwesomeIcon;
        icons.some(icon => {
            if (icon.id === id) {
                result = icon;
                return true;
            }
        });
        return result;
    }

    private canChangeIcon(): boolean {
        return `fa-${this.selectedIcon.id}` !== this.getCurrentIcon() || this.currentColour !== this.selectedColour;
    }

    private getIconStyle(colour: string): object {
        return colour ? {color: colour} : {};
    }

    private getPaletteStyle(colour: string): object {
        return colour ? {'background-color': colour} : {};
    }

    private onSelectColour(colour: string): void {
        this.selectedColour = colour;
    }
}
