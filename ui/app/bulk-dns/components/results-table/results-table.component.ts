import {Component, Input} from '@angular/core';
import BatchResult from '../../models/BatchResult';
import * as _ from 'lodash';

@Component({
    selector   : 'results-table',
    templateUrl: './results-table.template.html',
    styleUrls  : ['./results-table.styles.scss']
})
export default class ResultsTableComponent {
    @Input() private results: BatchResult[];

    private showWhoisModal = false;
    private showGeoModal = false;
    private whoisResult: BatchResult;
    private geoResult: BatchResult;
    private allSelected = true;

    private onShowRaw(result: BatchResult): void {
        this.whoisResult = result;
        this.showWhoisModal = true;
    }

    private onWhoisModalCancel(): void {
        this.whoisResult = null;
        this.showWhoisModal = false;
    }

    private getGeoFields(result: BatchResult): string[] {
        const geoFields: string[] = [];

        if (result && result.geoip) {
            geoFields.push(
                result.geoip.city,
                result.geoip.country
            );
        }

        return geoFields.filter(_.identity);
    }

    private canDisplayGeo(result: BatchResult): boolean {
        if (!result || !result.geoip) {
            return false;
        }

        return !!result.geoip.city
            || !!result.geoip.country
            || (result.geoip.ll && result.geoip.ll.length >= 2);
    }

    private onShowGeo(result: BatchResult): void {
        this.geoResult = result;
        this.showGeoModal = true;
    }

    private onGeoModalCancel(): void {
        this.geoResult = null;
        this.showGeoModal = false;
    }

    private selectAll(): void  {
        this.allSelected = this.allSelected ? false : true;
        for (const result of this.results) {
            result.isSelected = this.allSelected;
        }
    }

    private select(result: BatchResult): void  {
        result.isSelected = result.isSelected ? false : true;
        this.checkIfAllSelected();
    }

    private checkIfAllSelected(): void {
        let allSelected = true;
        for (const result of this.results) {
            if (!result.isSelected) {
                allSelected = false;
                break;
            }
        }
        this.allSelected = allSelected;
    }
}
