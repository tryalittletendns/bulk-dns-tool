import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector   : 'poc-list',
    templateUrl: './poc-list.template.html',
    styleUrls  : ['./poc-list.styles.scss']
})
export default class PocListComponent {
    @Input() private pocs: string[];

    @Input() private pocClass: string;

    @Input() private editMode: boolean;

    @Output() private remove = new EventEmitter<string>();

    private onPocClick(poc: string): void {
        if (this.editMode) {
            this.remove.emit(poc);
        }
    }
}
