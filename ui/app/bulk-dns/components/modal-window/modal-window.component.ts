import {Component, OnDestroy, OnInit} from '@angular/core';
import AbstractModalFormComponent from '../abstract/abstract-modal-form.component';

@Component({
    selector   : 'modal-window',
    templateUrl: './modal-window.template.html',
    styleUrls  : ['./modal-window.styles.scss']
})
export default class ModalWindowComponent extends AbstractModalFormComponent implements OnInit, OnDestroy {
    private ready = false;

    public ngOnInit(): void {
        document.getElementsByTagName('html')[0].classList.add('no-scroll');
        setTimeout(() => this.ready = true);
    }

    public ngOnDestroy(): void {
        document.getElementsByTagName('html')[0].classList.remove('no-scroll');
    }
}
