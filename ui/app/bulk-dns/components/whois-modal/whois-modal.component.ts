import {Component, Input} from '@angular/core';
import AbstractModalFormComponent from '../abstract/abstract-modal-form.component';

@Component({
    selector   : 'whois-modal',
    templateUrl: './whois-modal.template.html',
    styleUrls  : ['./whois-modal.styles.scss']
})
export default class WhoisModalComponent extends AbstractModalFormComponent {
    @Input() private searchTerm: string;
    @Input() private whois: string;
}
