import {Component} from '@angular/core';

import appConfig from '../../config/appConfig';

import '!style-loader!css-loader!sass-loader!./login-page.styles.scss';

@Component({
    templateUrl: './login-page.template.html'
})
export default class LoginPageComponent {
    private appName = appConfig.appName;

    private googleUrl = `${appConfig.service.url}/auth/google`;
}
