import {Component, Input, Output, OnChanges, SimpleChanges, EventEmitter} from '@angular/core';
import BatchResult from '../../models/BatchResult';
import AbstractModalFormComponent from '../abstract/abstract-modal-form.component';
import ApiService from '../../services/api.service';

@Component({
    selector   : 'extend-expiry-modal',
    templateUrl: './extend-expiry-modal.template.html',
    styleUrls  : ['./extend-expiry-modal.styles.scss']
})
export default class ExtendExpiryComponent extends AbstractModalFormComponent {
    @Input() private batchId: string;
    private selectedTtl: number = 600000;

    @Output() private complete = new EventEmitter<void>();

    constructor(private api: ApiService) {
        super();
    }

    private onExtend(): void {
        this.api.extendBatch(this.batchId, this.selectedTtl)
            .subscribe(() => {
                 this.complete.emit();
            });
    }
}
