import {Component, Input, OnInit} from '@angular/core';
import appConfig from '../../config/appConfig';
import {Headers, Http, ResponseContentType} from '@angular/http';
import {WindowRef} from '../../services/window-ref.service';

@Component({
    selector   : 'feedback-button',
    templateUrl: './feedback-button.template.html',
    styleUrls  : ['./feedback-button.styles.scss']
})
export default class FeedbackButtonComponent implements OnInit {
    private collectorUrl = appConfig.issueCollectorUrl;
    private window: any;
    private showCollectorDialog: () => void;

    @Input() private styleClass: string;

    constructor(private http: Http, windowRef: WindowRef) {
        if (!this.collectorUrl) {
            return;
        }

        this.window = windowRef.nativeWindow;

        this.window.ATL_JQ_PAGE_PROPS = {
            triggerFunction: (showCollectorDialog: () => void) => {
                this.showCollectorDialog = showCollectorDialog;
            }
        };
    }

    public ngOnInit(): void {
        const script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = this.collectorUrl;
        document.getElementsByTagName('head')[0].appendChild(script);
    }

}
