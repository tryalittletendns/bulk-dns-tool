import {Component, OnInit} from '@angular/core';
import ApiService from '../../services/api.service';
import {NavigationEnd, Router} from '@angular/router';
import * as _ from 'lodash';

@Component({
    selector   : 'app',
    templateUrl: './app.template.html',
    styleUrls  : ['./app.styles.scss']
})
export default class AppComponent implements OnInit {
    private ready = false;

    constructor(private api: ApiService, private router: Router) {
        router.events.subscribe(e => {
            if (e instanceof NavigationEnd) {
                if (!e.url.startsWith('/login')) {
                    api.getCurrentUser()
                        .subscribe(_.noop);
                }
            }
        });
    }

    public ngOnInit(): void {
        setTimeout(() => {
            this.ready = true;
        });
    }
}
