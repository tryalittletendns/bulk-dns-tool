import {Component, OnDestroy} from '@angular/core';
import ApiService from '../../services/api.service';
import {ActivatedRoute} from '@angular/router';
import Batch from '../../models/Batch';
import * as Papa from 'papaparse';

const pollingInterval = 2000;

@Component({
    templateUrl: './results-page.template.html',
    styleUrls  : ['./results-page.styles.scss']
})
export default class ResultsPageComponent implements OnDestroy {
    private batchId: string;
    private pollTimeout: number;
    private batch: Batch;
    private showExtendExpiryModal = false;

    constructor(private api: ApiService, route: ActivatedRoute) {
        route.params.subscribe(params => {
            if (params.id) {
                this.batchId = params.id;
                this.poll();
            }
        });
    }

    public ngOnDestroy(): void {
        if (this.pollTimeout) {
            window.clearTimeout(this.pollTimeout);
        }
    }

    private poll(): void {
        this.api.getBatch(this.batchId)
            .subscribe(batch => {
                this.batch = batch;
                if (!this.batch || !this.batch.complete) {
                    this.pollTimeout = window.setTimeout(this.poll.bind(this), pollingInterval);
                } else {
                    for (const result of this.batch.results) {
                        result.isSelected = true;
                    }
                }
            });
    }

    private getHeading(): string {
        return `Batch Results${(this.batch && this.batch.screenName) ? ': ' + this.batch.screenName : ''}`;
    }

    private onCsvExport(): void {
        const fileName = 'bulk-identifier-lookup-' + this.batch._id + '.csv';

        const columns = [
            'source time',
            'feeder',
            'feed',
            'ip',
            'domain name',
            'type',
            'malware family',
            'description',
            'comment'
        ];

        const csvData = [columns];

        this.batch.results.forEach(result => {
            if (result.isSelected) {
                result.hostnames.forEach(hostname => {
                    csvData.push([
                        null,
                        'ncsc',
                        'jointops',
                        result.ip,
                        hostname,
                        'c&c',
                        null,
                        'This host is most likely a command and control server.',
                        null
                    ]);
                });
            }
        });

        this.downloadText(fileName, Papa.unparse(csvData));
    }

    private onEmailExport(): void {
        const fileName = 'bulk-identifier-emails-' + this.batch._id + '.csv';

        const csvData = [[
            'identifier',
            'email'
        ]];

        this.batch.results.reduce((emailData, result) => {
            if (result.isSelected) {
                const sourceEmailPairs = result.abuseEmail.map(email => ({
                    identifier: result.identifier,
                    email
                }));
                emailData.push(...sourceEmailPairs);
            }
            return emailData;
        }, []).map(emailData => [
            emailData.identifier,
            emailData.email
        ]).forEach(row => csvData.push(row));

        this.downloadText(fileName, Papa.unparse(csvData));
    }

    private downloadText(filename: string, text: string): void {
        const base64 = btoa(text);
        const dataUrl = 'data:text/plain;base64,' + base64;

        const link = document.createElement('a');
        link.download = filename;
        link.href = dataUrl;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    private anyResultsSelected(): boolean {
        if (this.batch) {
            return this.batch.results.some(result => result.isSelected);
        }
        return false;
    }

    private onExtendExpiry(): void {
        this.showExtendExpiryModal = true;
    }

    private onExtendExpiryCancel(): void {
        this.showExtendExpiryModal = false;
    }

    private onExtendComplete(): void {
        this.showExtendExpiryModal = false;
        this.poll();
    }
}
