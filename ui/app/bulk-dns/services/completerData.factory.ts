import {Http} from '@angular/http';

import CustomRemoteData from '../models/CustomRemoteData';

export function customRemoteDataFactory(http: Http): () => CustomRemoteData {
    return () => {
        return new CustomRemoteData(http);
    };
}

export let CustomRemoteDataFactoryProvider = {
    provide   : CustomRemoteData,
    useFactory: customRemoteDataFactory,
    deps      : [Http]
};
