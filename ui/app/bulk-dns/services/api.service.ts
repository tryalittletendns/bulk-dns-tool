import {Injectable} from '@angular/core';
import {Http, Response, RequestOptions, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';

import appConfig from '../config/appConfig';
import User from '../models/User';
import {Router} from '@angular/router';
import Batch from '../models/Batch';

const defaultOptions = new RequestOptions({
    withCredentials: true
});

@Injectable()
export default class ApiService {

    private user: User;

    constructor(private http: Http, private router: Router) {
    }

    /********
     * User *
     ********/

    public getCurrentUser(): Observable<User> {
        if (this.user) {
            return Observable.of(this.user);
        } else {
            return this.getData('user')
                .map(user => this.user = user as User);
        }
    }

    /***********
     * Batches *
     ***********/

    public createBatch(searchTerms: string[]): Observable<Batch> {
        return this.postData('/batch', searchTerms);
    }

    public getBatches(): Observable<Batch[]> {
        return this.getData('/batches')
            .map((batches: Batch[]) => {
                return _.orderBy(batches, ['dateStarted'], ['desc']);
            });
    }

    public getBatch(batchId: string): Observable<Batch> {
        return this.getData(`/batch/${batchId}`);
    }

    public extendBatch(batchId: string, extendBy: number): Observable<Batch> {
        const options = new RequestOptions({
            withCredentials: true
        });
        if (extendBy) {
            options.search = new URLSearchParams();
            options.search.append('extendBy', extendBy.toString());
        }
        return this.putData(`/batch/${batchId}/extend`, null, options);
    }


    /*********
     * Other *
     *********/

    public getDefaultRequestOptions(): RequestOptions {
        return new RequestOptions(JSON.parse(JSON.stringify(defaultOptions)));
    }

    /*******************
     * Private Methods *
     *******************/

    private getData<T>(path: string, options?: RequestOptions): Observable<T> {
        return this.http.get(`${appConfig.service.url}/${path}`, this.generateRequestOptions(options))
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    }

    private postData(path: string, data?: object, options?: RequestOptions): Observable<any> {
        return this.http.post(`${appConfig.service.url}/${path}`, data, this.generateRequestOptions(options))
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    }

    private putData(path: string, data: object, options?: RequestOptions): Observable<any> {
        return this.http.put(`${appConfig.service.url}/${path}`, data, this.generateRequestOptions(options))
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    }

    private deleteData(path: string, options?: RequestOptions): Observable<any> {
        options = options || defaultOptions;
        return this.http.delete(`${appConfig.service.url}/${path}`, this.generateRequestOptions(options))
            .map(this.extractData)
            .catch(this.handleError.bind(this));
    }

    private generateRequestOptions(options?: RequestOptions): RequestOptions {
        return options || defaultOptions;
    }

    private extractData<T>(res: Response): T {
        try {
            return res.json() as T;
        } catch (err) {
            return null;
        }
    }

    private handleError<T>(err: Response | any): Observable<T> {
        let message: string;

        if (err instanceof Response) {
            if (err.status === 401) {
                this.router.navigate(['/login']);
            }

            message = `${err.status}: ${err.statusText}`;
        } else {
            message = err.message || err.toString();
        }

        console.error(message);

        return Observable.throw(err);
    }
}
