import {CompleterService, LocalData} from 'ng2-completer';
import {Inject, Injectable} from '@angular/core';
import CustomRemoteData from '../models/CustomRemoteData';

@Injectable()
export default class CustomCompleterService extends CompleterService {
    constructor(@Inject(LocalData) private childLocalDataFactory: any,
                @Inject(CustomRemoteData) private childRemoteDataFactory: any) {
        super(childLocalDataFactory, childRemoteDataFactory);
    }

    public remote(url: string | null, searchFields: string | null = '',
                  titleField: string | null                       = ''): CustomRemoteData {
        return super.remote(url, searchFields, titleField) as CustomRemoteData;
    }
}
