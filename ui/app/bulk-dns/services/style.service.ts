import {Injectable} from '@angular/core';
import Style from '../models/Style';

const rawVizColours = require('!raw-loader!../../scss/_viz-colours.scss'),
      vizColours    = rawVizColours.match(/#([a-fA-F0-9]+)/g),
      iconPrimary   = require('../../assets/images/placeholder-logo.png'),
      iconSecondary = require('../../assets/images/placeholder-logo-secondary.png');

const routeMap: { [key: string]: Style } = {
    home : Style.PRIMARY,
    flow : Style.PRIMARY,
    admin: Style.SECONDARY
};

@Injectable()
export default class StyleService {
    public getVizColours(): string[] {
        return vizColours.slice();
    }

    public getStyleClass(style: Style): string {
        switch (style) {
            case Style.PRIMARY:
                return 'primary';
            case Style.SECONDARY:
                return 'secondary';
            case Style.TERTIARY:
                return 'tertiary';
            default:
                return '';
        }
    }

    public getStyleForRoute(route: string): Style {
        return routeMap[route];
    }

    public getStyleIcon(style: Style): string {
        switch (style) {
            case Style.PRIMARY:
                return iconPrimary;
            case Style.SECONDARY:
                return iconSecondary;
            default:
                return '';
        }
    }
}
