import BatchResult from './BatchResult';

interface Batch {
    _id: string;
    screenName: string;
    searchItems: string[];
    complete: boolean;
    dateStarted: string;
    dateComplete: string;
    results: BatchResult[];
    ttl: string;
}

export default Batch;
