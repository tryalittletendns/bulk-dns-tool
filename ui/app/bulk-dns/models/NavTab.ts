import Style from './Style';

interface NavTab {
    title: string;
    path: string;
    style: Style;
}

export default NavTab;
