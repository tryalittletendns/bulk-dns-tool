interface User {
    _id?: string;
    authId: string;
    name: string;
    email?: string;
    admin?: boolean;
}

export default User;
