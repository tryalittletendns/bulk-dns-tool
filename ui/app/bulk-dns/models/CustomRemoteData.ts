import {RemoteData} from 'ng2-completer/services/remote-data';
import {CompleterItem} from 'ng2-completer/components/completer-item';
import {Headers, Http, RequestOptions} from '@angular/http';
import * as _ from 'lodash';

export default class CustomRemoteData extends RemoteData {
    private _itemDecorator: (item: CompleterItem) => CompleterItem = _.identity;

    constructor(private childHttp: Http) {
        super(childHttp);
    }

    public urlFormater(urlFormater: (term: string) => string): CustomRemoteData {
        super.urlFormater(urlFormater);
        return this;
    }

    public dataField(dataField: string): CustomRemoteData {
        super.dataField(dataField);
        return this;
    }

    public headers(headers: Headers): CustomRemoteData {
        super.headers(headers);
        return this;
    }

    public requestOptions(requestOptions: RequestOptions): CustomRemoteData {
        super.requestOptions(requestOptions);
        return this;
    }

    public itemDecorator(itemDecorator: (item: CompleterItem) => CompleterItem): CustomRemoteData {
        this._itemDecorator = itemDecorator;
        return this;
    }

    public convertToItem(data: any): CompleterItem | null {
        const item = super.convertToItem(data);

        return this._itemDecorator(item);
    }
}
