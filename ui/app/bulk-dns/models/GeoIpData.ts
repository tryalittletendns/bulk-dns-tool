interface GeoIpData {
    city?: string;
    country?: string;
    ll?: number[];
    metro?: number;
    range?: number[];
    region?: string;
    zip?: number;
}

export default GeoIpData;
