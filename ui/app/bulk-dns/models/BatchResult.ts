import GeoIpData from './GeoIpData';

interface BatchResult {
    identifier: string;
    abuseEmail: string[];
    whois: string;
    hostnames: string[];
    ip: string;
    geoip: GeoIpData;
    isSelected: boolean;
}

export default BatchResult;
