interface FontAwesomeIcon {
    name: string;
    id: string;
    unicode: string;
    created: number;
    filter?: string[];
    categories: string[];
}

export default FontAwesomeIcon;
