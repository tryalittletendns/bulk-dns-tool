# Bulk DNS Lookup Tool

## Dev Quickstart

Make sure you have a local MongoDB instance running on the default port (27017).

Provided you have the Node and npm installed, the following should install all necessary dependencies and set up your database and config files for development.

```
 >  npm install
 >  npm run setup
```

You should now have a `config.json` file located in the `service` directory. If you are developing at Tommy's, then in order to allow authentication with Google, you will need to add the Google client secret for Bulk DNS Lookup Tool to this config file (in the `google.clientSecret` field). You can find this by logging in to the [Google APIs Console](https://console.developers.google.com/apis/dashboard).

If no Google APIs project has been created, you will need to create a new project, and create a new OAuth client ID for that project from the credentials tab in the API Manager. Once an OAuth client ID has been created for the app, you will need to add `http://localhost:8080` to the authorised javascript origins, and `http://localhost:3000/api/auth/google/callback` to the authorised redirect URIs (if deploying to AWS, you will also need to add the same paths for the AWS hostname).

Once an OAuth client ID has been created for the app, you will be able to find the client ID and client secret for the project in the credentials tab of the API Manager.

If the bulk-dns-tool project exists, but you don't have access to it (it doesn't appear in the project selector), contact a member of the Bulk DNS Lookup Tool development team.
 
Once authentication has been configured, you can run the application in development mode using:
 
 ```
 > npm run dev
 ```

## Versioning

To release a new version, run the following on master:

```
 > npm version <new_version>
```

`<new_version>` should either be a version number or one of `patch`, `minor`, `major`, `prepatch`, `preminor`, `premajor` or `prerelease`, as per the [npm-version documentation](https://docs.npmjs.com/cli/version). This will update the relevant `package.json` files and create a tagged commit. You should then push the version to origin:

```
 > git push --follow-tags origin master
```
