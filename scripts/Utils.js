import _fs from 'fs';
import path from 'path';

import Promise from 'bluebird';

const fs = Promise.promisifyAll(_fs);

const modelsPath = path.resolve(__dirname, '../service/src/db/model'),
      dataPath   = path.resolve(__dirname, 'data');

class Utils {
    getModels() {
        return fs.readdirAsync(modelsPath)
            .then(files => files.map(file => require(path.join(modelsPath, file)).default));
    }

    getData() {
        return fs.readdirAsync(dataPath)
            .then(files => {
                const data = {};
                files.forEach(file => {
                    data[path.basename(file, path.extname(file))] = require(path.join(dataPath, file));
                });
                return data;
            });
    }
}

export default new Utils();
