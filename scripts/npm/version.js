const _fs            = require('fs'),
      path           = require('path'),
      Promise        = require('bluebird'),
      fs             = Promise.promisifyAll(_fs),
      git            = require('simple-git')(__dirname),
      packageJson    = require('../../package.json'),
      newVersion     = packageJson.version,
      uiPackage      = require('../../ui/package.json'),
      servicePackage = require('../../service/package.json');

uiPackage.version = newVersion;
servicePackage.version = newVersion;

Promise.all([
    fs.writeFileAsync(path.resolve(__dirname, '../../ui/package.json'), JSON.stringify(uiPackage, null, 2)),
    fs.writeFileAsync(path.resolve(__dirname, '../../service/package.json'), JSON.stringify(servicePackage, null, 2))
]).then(() => {
    git.add([
        path.resolve(__dirname, '../../ui/package.json'),
        path.resolve(__dirname, '../../service/package.json')
    ]);
});
