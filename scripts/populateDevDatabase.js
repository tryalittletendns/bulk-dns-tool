import Promise from 'bluebird';

import log from '../service/src/util/log';
import DatabaseService from '../service/src/util/DatabaseService';

import Utils from './Utils';

populateDevDatabase();

function populateDevDatabase() {
    DatabaseService.connect()
        .then(() => Promise.all([Utils.getModels(), Utils.getData()]))
        .then(populate)
        .then(DatabaseService.disconnect);
}

function populate([models, data]) {
    return Promise.map(models, model => {
        let sampleData = model && model.modelName && data[model.modelName];
        if (sampleData) {
            log.info(`Inserting sample data for collection "${model.collection.collectionName}"`);
            sampleData = sampleData instanceof Array ? sampleData : [sampleData];
            return Promise.map(sampleData, datum => {
                return new model(datum).save();
            });
        } else {
            return Promise.resolve();
        }
    });
}
