import Promise from 'bluebird';

import log from '../service/src/util/log';
import DatabaseService from '../service/src/util/DatabaseService';

import Utils from './Utils';

cleanDatabase();

function cleanDatabase() {
    DatabaseService
        .connect()
        .then(Utils.getModels)
        .then(dropCollections)
        .then(DatabaseService.disconnect);
}

function dropCollections(models) {
    return Promise.map(models, model => {
        if (model.collection) {
            const collectionName = model.collection.collectionName;
            log.info(`Dropping collection "${collectionName}"`);
            return model.collection
                .drop()
                .catch(err => {
                    if (err.message === 'ns not found') {
                        log.info(`No collection "${collectionName}" to drop. Proceeding...`);
                    } else {
                        log.error(err);
                    }
                });
        }
        return Promise.resolve();
    });
}
