import _ from 'lodash';
import User from '../model/User';

/*
 * Base Collection object that controlls the interaction to the mongo database. 
 */
class UserCollection {
    constructor() {
        this.model = User;
    }

    /*
     * Conduct a query to return objects from the database.
     *
     * query to run on the database
     * projection to clean the responce with
     */
    find(query, projection) {
        query = query || {};
        projection = projection || this.getDefaultProjection();

        return this.model
            .find(query, projection)
            .exec()
            .then(results => {
                return results;
            });
    }

    /*
     * Find a single object by it's _id
     *
     * id of the object to return.
     * projection to clean the responce with
     */
    findById(id, projection) {
        const query = {_id: id};
        projection = projection || this.getDefaultProjection();

        return this.model
            .findOne(query, projection)
            .exec()
            .then(result => {
                return result;
            });
    }

    /*
     * Find or create a user from the supplied raw user data
     */
    findOrCreate(user) {
        const query = {};
        if (user._id) {
            query._id = user._id;
        } else if (user.authId) {
            query.authId = user.authId;
        } else if (user.name) {
            query.name = user.name;
        }

        return this.model
            .findOne(query)
            .exec()
            .then(result => {
                if (result) {
                    return result;
                } else {
                    _.merge(user, {admin: false});
                    const Model = this.model,
                          model = new Model(user);
                    return model.save();
                }
            });
    }

    /*
     * Find users based on a search string
     */
    findByTextSearch(search, limit) {
        let query = {};
        if (search) {
            query = {
                $or: [
                    {name: new RegExp(search, 'i')},
                    {email: new RegExp(search, 'i')}
                ]
            };
        }

        const queryBuilder = this.model.find(query, this.getDefaultProjection());

        if (limit) {
            queryBuilder.limit(limit);
        }

        return queryBuilder.exec();
    }

    /*
     * Create object within the database.
     *
     * modelData that holds the data to persist.
     */
    create(modelData) {
        const Model = this.model,
              model = new Model(modelData);
        return model.save();
    }

    /*
     * Update model without checking if the user is allowed.
     *
     * id of the object to update.
     * modelData that holds the data to persist.
     */
    update(id, modelData) {
        const Model = this.model,
              model = new Model(modelData);
        return this.model.update({_id: id || model._id}, model);
    }

    /*
     * The default projection to use that clears out all fields that are not in the schema. 
     */
    getDefaultProjection() {
        const obj = this.model.schema.obj;
        return _.mapValues(obj, _.constant(true));
    }
}

export default new UserCollection();
