import Collection from './Collection';
import Batch from '../model/Batch';

class BatchCollection extends Collection {
    constructor() {
        super(Batch);
    }
}

export default new BatchCollection();