import _ from 'lodash';

/*
 * Base Collection object that controlls the interaction to the mongo database. 
 */
export default class Collection {
    constructor(model) {
        this.model = model;
    }

    /*
     * Conduct a query to return objects from the database.
     *
     * query to run on the database
     * projection to clean the responce with
     */
    find(query, projection, sort, skip, limit) {
        query = query || {};
        projection = projection || this.getDefaultProjection();
        sort = sort || {};
        const model = this.model
            .find(query, projection)
            .sort(sort);

        if (limit) {
            model.limit(parseInt(limit))
        }

        if (skip) {
            model.skip(parseInt(skip))
        }

        return model.exec()
            .then(results => {
                return results;
            });
    }

    findOne(query, projection) {
        query = query || {};
        projection = projection || this.getDefaultProjection();

        return this.model
            .findOne(query, projection)
            .exec();
    }

    /*
     * Find a single object by it's _id
     *
     * id of the object to return.
     * projection to clean the responce with
     */
    findById(id, projection) {
        const query = {_id: id};
        projection = projection || this.getDefaultProjection();

        return this.model
            .findOne(query, projection)
            .exec()
            .then(result => {
                return result;
            });
    }

    /*
     * Count the number of records returned from the count.
     *
     * query to count results from.
     */
    count(query) {
        return this.model
            .count(query)
            .exec()
            .then(result => {
                return result;
            });
    }

    /*
     * Create object within the database.
     *
     * modelData that holds the data to persist.
     */
    create(modelData) {
        const Model = this.model,
              model = new Model(modelData);
        return model.save();
    }

    /*
     * Update object within the database.
     *
     * id of the object to update.
     * modelData that holds the data to persist.
     */
    update(id, modelData, userId) {
        const Model = this.model,
              model = new Model(modelData);
        return this.model.update({_id: id || model._id}, model);
    }

    canUserEdit(userId, result) {
        if (!userId) {
            return false
        } else if (userId.toString() === result.createdBy.toString()) {
            return true;
        } else {
            for (let i = 0; i < result.pointsOfContact.length; i++) {
                if (userId.toString() === result.pointsOfContact[i].toString()) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
     * Update model without checking if the user is allowed.
     *
     * id of the object to update.
     * modelData that holds the data to persist.
     */
    updateWithoutUserCheck(id, modelData) {
        const Model = this.model,
              model = new Model(modelData);
        return this.model.update({_id: id || model._id}, model);
    }

    /*
     * Delete object within the database.
     *
     * id of the object to delete.
     * userId of the user conducting the delete.
     */
    delete(id, userId) {
        return this.model
            .remove({_id: id})
            .exec()
            .then(result => {
                return result;
            });
    }

    /*
     * Update single field within an object in the database.
     *
     * id of the object to update.
     * field to update.
     * value to update the field with.
     */
    updateField(id, field, value) {
            // Validate field using mongoose
            const a = {};
            a[field] = value;
            const Model = this.model,
                  model = new Model(a);
            var error = model.validateSync();
            if (error && error.errors && error.errors[field]) {
                return Promise.reject(error.errors[field]);
            } else {
                const updateModel = {$set: {}};
                updateModel.$set[field] = value;
                return this.model.update({_id: id}, updateModel);
            }
    }

    /*
     * Increment a field within an object.
     *
     * id of the object to update.
     * field to increment.
     */
    increment(id, field) {
        return this.incrementFieldBy(id, field, 1);
    }

    /*
     * Decrease a field within an object.
     *
     * id of the object to update.
     * field to decrease.
     */
    decrease(id, field) {
        return this.incrementFieldBy(id, field, -1);
    }

    /*
     * Increment a field within an object by a given value.
     *
     * id of the object to update.
     * field to increment.
     * value to increment by (negative to decrease field value)
     */
    incrementFieldBy(id, field, value) {
        value = value || 1;

        const query = {$inc: {}};
        query.$inc[field] = value;
        return this.model.update({_id: id}, query);
    }

    /*
     * Get the enumeration values for the given field.
     *
     * field to get valid enumeration values for.
     */
    getEnumValues(field) {
        const Model = this.model,
              model = new Model();
        return Promise.resolve(model.schema.path(field).enumValues);
    }

    /*
     * The default projection to use that clears out all fields that are not in the schema. 
     */
    getDefaultProjection() {
        const obj = this.model.schema.obj;
        return _.mapValues(obj, _.constant(true));
    }

    getTags(query, projection) {
        return this.model
            .aggregate([
                {
                    $project : {
                        _id : 0,
                        tags : 1
                }},
                {
                    $unwind : "$tags"
                },
                {
                    $group : {
                        _id: "$tags",
                        count: {
                            $sum : 1
                        }
                    }
                },
                {
                    $project : {
                        _id : 0,
                        tag: "$_id",
                        count: 1
                    }
                }
            ])
            .exec();
    }
}
