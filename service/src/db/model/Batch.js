import mongoose from 'mongoose';

import BatchSchema from '../schema/BatchSchema';

const Batch = mongoose.model('batch', BatchSchema);

export default Batch;
