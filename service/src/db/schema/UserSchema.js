import {Schema} from 'mongoose';
import AuthenticationService from '../../auth/AuthenticationService';

const schemaDefinition = {
    authId: String,
    name  : String,
    email : String,
    admin : Boolean
};

const UserSchema = new Schema(schemaDefinition);

AuthenticationService.configureSchema(UserSchema);

export default UserSchema;
