import {Schema} from 'mongoose';
import appConfig from '../../config/appConfig';

const schemaDefinition = {
        screenName : String,
        searchItems : [ String ],
        complete  : Boolean,
        dateStarted : {
            type    : Date,
            default : Date.now
        },
        dateComplete : Date,
        ttl : {
            type    : Date,
            expires : 60
        },
        results :[Schema.Types.Mixed],
        emailAddresses : [{
            domain : String,
            emailAddresses : [String]
        }]
};

const BatchSchema = new Schema(schemaDefinition);

export default BatchSchema;
