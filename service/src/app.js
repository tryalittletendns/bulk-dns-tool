import packageJson from '../package.json';
import cluster from 'cluster';
import os from 'os';
import Server from './server/Server';

import 'babel-polyfill';

import DatabaseService from './util/DatabaseService';
import log from './util/log';
import SecretService from './util/SecretService';

if (cluster.isMaster) {
    log.info(`Tommy's Totaliser v${packageJson.version} starting...`);
    log.info(`Master process ${process.pid} started`);

    SecretService.init()
        .then(() => {
            os.cpus().forEach(() => cluster.fork());

            cluster.on('disconnect', (worker) => {
                log.warn(`Worker ${worker.process.pid} disconnected`);
                cluster.fork();
            });
        });
} else {
    const server = new Server();
    process.on('uncaughtException', (err) => {
        log.error(`Uncaught Exception - worker ${process.pid} exiting`);
        terminateWorker(server, err);
    });

    startWorker(server);
}

function startWorker(server) {
    DatabaseService.connect()
        .then(() => {
            server.start();

            log.info(`Worker ${process.pid} started`);
        })
        .catch(err => {
            log.warn('Failed to establish database connection');
            terminateWorker(server, err);
        });
}

function terminateWorker(server, err) {
    try {
        const killTimer = setTimeout(() => process.exit(1), 30000);
        killTimer.unref();

        log.error(err);

        server.stop();
        DatabaseService.disconnect();

        cluster.worker.disconnect();
    } catch (err2) {
        console.error(err2);
        console.error(`Error stopping server. Worker ${process.pid} will exit.`);
    }
}
