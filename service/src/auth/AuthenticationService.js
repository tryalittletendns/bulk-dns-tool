import _fs from 'fs';
import path from 'path';

import Promise from 'bluebird';

import appConfig from '../config/appConfig';
import log from '../util/log';

const fs = Promise.promisifyAll(_fs);

class AuthenticationService {
    configureSchema(UserSchema) {
        this.getStrategy().configureSchema(UserSchema);
    }

    configurePassport(User) {
        this.getStrategy().configurePassport(User);
    }

    getStrategy() {
        if (!this.strategy) {
            listStrategies().some(strategy => {
                return strategy.key === appConfig.authenticationStrategy && (this.strategy = strategy);
            });
            log.info(`Using authentication strategy "${this.strategy.key}"`);
        }

        return this.strategy;
    }

    configureAuthRoutes(app) {
        this.getStrategy().configureRoutes(app);
    }
}

export default new AuthenticationService();

function listStrategies() {
    const strategyDir = path.resolve(__dirname, 'strategy'),
          files       = fs.readdirSync(strategyDir);
    return files.map(file => require(path.join(strategyDir, file)).default);
}
