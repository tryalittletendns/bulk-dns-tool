import passport from 'passport';
import basicAuthMongoose from 'basic-auth-mongoose';
import passportHttp from 'passport-http';

import appConfig from '../../config/appConfig';
import AuthenticationStrategy from '../AuthenticationStrategy';

const BasicStrategy = passportHttp.BasicStrategy;

class LocalAuthenticationStrategy extends AuthenticationStrategy {
    constructor() {
        super('basic');
    }

    configureSchema(UserSchema) {
        UserSchema.plugin(basicAuthMongoose);
    }

    configurePassport(User) {
        passport.use(new BasicStrategy((username, password, done) => {
            User.findOne({username: username})
                .then(user => {
                    if (user && user.authenticate(password)) {
                        done(null, user);
                    } else if (!user && appConfig.autoCreateUser) {
                        user = new User({
                            name : username,
                            username,
                            password,
                            admin: false
                        });
                        user.save().then(() => done(null, user));
                    } else {
                        done(null, false);
                    }
                })
                .catch(err => {
                    done(err);
                });
        }));
    }
}

export default new LocalAuthenticationStrategy();
