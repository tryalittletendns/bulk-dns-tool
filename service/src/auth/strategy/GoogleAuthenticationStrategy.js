import passport from 'passport';
import googleOauth from 'passport-google-oauth';
import jwt from 'jsonwebtoken';
import passportJwt from 'passport-jwt';
import GoogleOAuth2 from 'google-oauth2';
import moment from 'moment';
import _ from 'lodash';
import axios from 'axios';
import queryString from 'query-string';

import appConfig from '../../config/appConfig';
import AuthenticationStrategy from '../AuthenticationStrategy';
import SecretService from '../../util/SecretService';
import log from '../../util/log';
import User from '../../db/model/User';

const GoogleStrategy = googleOauth.OAuth2Strategy,
      JwtStrategy    = passportJwt.Strategy,
      goauth2        = new GoogleOAuth2({
          client_id    : appConfig.google.clientID,
          client_secret: appConfig.google.clientSecret,
          scope        : appConfig.google.scopes.join(' ')
      });

class GoogleAuthenticationStrategy extends AuthenticationStrategy {
    constructor() {
        super('google');
    }

    configureSchema(UserSchema) {
        UserSchema.add({
            refreshToken: String,
            scopes      : [String],
            accessToken : String
        });
    }

    configurePassport(User) {
        SecretService.getJwtSecret()
            .then(secret => {
                passport.use(new GoogleStrategy({
                    clientID         : appConfig.google.clientID,
                    clientSecret     : appConfig.google.clientSecret,
                    callbackURL      : `http://${appConfig.server.host}:${appConfig.server.port}/api/auth/google/callback`,
                    passReqToCallback: true,
                    accessType       : 'offline'
                }, (req, accessToken, refreshToken, responseData, profile, done) => {
                    if (!checkEmail(profile.emails)) {
                        log.warn('Invalid email domain:');
                        log.warn(profile);
                        return done(null, false);
                    }

                    if (refreshToken) {
                        log.debug(`Refresh token for user ${profile.displayName} received`);
                    }

                    const authId = profile.id,
                          token  = {
                              id               : authId,
                              exp              : moment().add(appConfig.authCookieLifetimeHours, 'hours').unix(),
                              accessToken,
                              accessTokenExpiry: moment().add(responseData.expires_in, 'seconds').valueOf()
                          };

                    req.token = jwt.sign(token, secret);

                    findUser(User, authId, accessToken, refreshToken, {
                        authId,
                        name : profile.displayName,
                        email: findEmail(profile),
                        admin: false
                    }).then(user => done(null, user));
                }));

                passport.use(new JwtStrategy({
                    jwtFromRequest: req => {
                        return req && req.cookies && req.cookies[appConfig.authCookieName];
                    },
                    secretOrKey   : secret
                }, (token, done) => {
                    const id = token.id;
                    if (!id) {
                        return done(null, false);
                    }

                    findUser(User, id).then(user => done(null, user));
                }));
            });

        passport.serializeUser((user, done) => done(null, user));
        passport.deserializeUser((user, done) => done(null, user));
    }

    authenticateApi(options) {
        return (req, res, next) => {
            passport.authenticate('jwt', options)(req, res, () => {
                const token        = getTokenFromCookies(req),
                      user         = req.user,
                      refreshToken = user.refreshToken;

                if (!refreshToken) {
                    log.debug(`No refresh token attached to user ${user.authId}`);
                    return next();
                }

                if (!token.accessToken || isExpired(token.accessTokenExpiry)) {
                    log.debug(`Access token for user ${user.authId} has expired. Requesting new access token.`);
                    renewAccessToken(refreshToken)
                        .then(result => {
                            if (result.access_token) {
                                token.accessToken = result.access_token;
                                token.accessTokenExpiry = moment().add(result.expires_in, 'seconds');
                                user.accessToken = result.access_token;
                                Promise.all([
                                    SecretService.getJwtSecret(),
                                    user.save()
                                ]).then((result) => {
                                    res.cookie(appConfig.authCookieName, jwt.sign(token, result[0]));
                                    return next();
                                });
                            } else {
                                return next();
                            }
                        })
                        .catch(err => {
                            log.error('Access token renewal failed');
                            log.error(err);
                            return res.status(401).end();
                        });
                } else {
                    return next();
                }
            });
        };
    }

    configureRoutes(app) {
        app.get('/auth/google', (req, res, next) => {
            const authConfig = {
                scope     : appConfig.google.scopes,
                hd        : 'tommys.tech',
                accessType: 'offline'
            };

            getUserFromCookies(req)
                .then(user => {
                    if (user) {
                        if (!user.refreshToken) {
                            log.debug(`No refresh token for user ${user.authId}. Forcing prompt.`);
                            authConfig.prompt = 'consent';
                        } else if (!checkScopes(user.scopes)) {
                            log.debug(`User scopes for user ${user.authId} are out of date. Forcing prompt.`);
                            authConfig.prompt = 'consent';
                        }
                    }
                    passport.authenticate('google', authConfig)(req, res, next);
                });
        });

        app.get('/auth/google/callback', passport.authenticate('google', {failureRedirect: '/login'}),
            (req, res) => {
                const token   = req.token,
                      expires = moment().add(appConfig.authCookieLifetimeHours, 'hours').toDate();
                res.cookie(appConfig.authCookieName, token, {expires})
                    .redirect(`http://${appConfig.ui.host}:${appConfig.ui.port}/splash`);
            }
        );
    }

    searchUsers(user, search, limit) {
        if (!user.accessToken) {
            return Promise.reject({status: 401});
        }

        const queryParameters = {
            maxResults: limit,
            domain    : 'tommys.tech',
            projection: 'full',
            viewType  : 'domain_public'
        };

        if (search) {
            queryParameters.query = search;
        }

        const url = `${appConfig.google.directoryApiUrl}/users?${queryString.stringify(queryParameters)}`;

        return axios.get(url, {
            headers: {
                Authorization: `Bearer ${user.accessToken}`
            }
        }).then(res => {
            if (!res.data || !res.data.users) {
                return [];
            }

            return Promise.all(res.data.users.map(googleUser => {
                const authId = googleUser.id;
                return User.findOne({authId})
                    .then(user => {
                        if (user) {
                            return user;
                        }

                        return {
                            authId,
                            name : googleUser.name.fullName,
                            email: googleUser.primaryEmail
                        };
                    });
            }));
        });
    }
}

export default new GoogleAuthenticationStrategy();

function checkEmail(emails) {
    if (!emails || !emails.length) {
        return false;
    }

    return emails.some(email => {
        return email.value && email.value.match(/tommys\.tech$/);
    });
}

function findEmail(profile) {
    if (!profile.emails || !profile.emails.length) {
        return '';
    }

    let result = '';

    profile.emails.some(email => {
        if (email.type === 'account') {
            return (result = email.value);
        }
    });

    if (!result) {
        result = profile.emails[0].value;
    }

    return result;
}

function checkScopes(scopes) {
    if (!scopes) {
        return false;
    }

    const intersection = _.intersection(appConfig.google.scopes, scopes);
    return intersection.length === appConfig.google.scopes.length && intersection.length === scopes.length;
}

function findUser(User, authId, accessToken, refreshToken, autoCreate) {
    return User.findOne({authId})
        .then(user => {
            if (!user) {
                if (autoCreate) {
                    user = new User(autoCreate);
                } else {
                    return false;
                }
            }

            if (accessToken) {
                user.accessToken = accessToken;
            }

            if (refreshToken) {
                user.refreshToken = refreshToken;
                user.scopes = appConfig.google.scopes;
            }

            if (autoCreate) {
                _.merge(user, autoCreate);
            }

            return user.save();
        });
}

function getTokenFromCookies(req) {
    if (!req || !req.cookies || !req.cookies[appConfig.authCookieName]) {
        return;
    }

    return jwt.decode(req.cookies[appConfig.authCookieName]);
}

function getUserFromCookies(req) {
    const token = getTokenFromCookies(req);

    if (!token || !token.id) {
        return Promise.resolve();
    }

    return findUser(User, token.id);
}

function isExpired(expiry) {
    if (!expiry) {
        return true;
    }

    return !moment(expiry).isAfter(moment());
}

function renewAccessToken(refreshToken) {
    return new Promise((resolve, reject) => {
        goauth2.getAccessTokenForRefreshToken(refreshToken, (err, result) => {
            if (err) {
                reject(err);
            }

            resolve(result);
        });
    });
}

function mapGoogleUser(userData) {
    return userData;
}
