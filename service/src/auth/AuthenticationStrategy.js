import passport from 'passport';

export default class AuthenticationStrategy {
    constructor(key) {
        this.key = key;
    }

    configureSchema() {
    }

    configurePassport() {
    }

    authenticateApi(options) {
        return passport.authenticate(this.key, options);
    }

    configureRoutes() {
    }

    searchUsers() {
        return Promise.resolve([]);
    }
}
