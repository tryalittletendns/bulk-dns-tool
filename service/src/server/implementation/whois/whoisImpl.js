import whois from 'node-whois';
import markMonitorExtractor from './whoisExtractors/markMonitor';
import markMonitorExtractorInt from './whoisExtractors/markMonitorInternational';
import arinExtractor from './whoisExtractors/arinWhois';
import ripeExtractor from './whoisExtractors/ripeExtractor';
import values from 'lodash/values';
import flow from 'lodash/flow';
import defaults from 'lodash/defaults';
import parseWhois from 'parse-whois';

// Lookup in whois content
const promisifiedLookup = (identifier) => new Promise((res, rej) => {
  whois.lookup(identifier, {
    follow : 4
  }, (e, d) => {
    if (e)
      return rej(e);

    res(d);
  })
});

// Turn an array of 2-length arrays into an object
const pairsToObject = (data) => data.reduce((obj, pair) => {
  if (!obj[pair[0]])
    obj[pair[0]] = [];

  obj[pair[0]].push(pair[1]);
  return obj;
}, {});

// Work out how many keys an object have which match to truthy values
const keyCount = (obj) => values(obj)
 .reduce((count, current) => {
   const increment = !!current
    ? 1
    : 0;

   return count + increment;
 }, 0);

// Parsers for different types of whois data
const extractors = [
  markMonitorExtractor,
  markMonitorExtractorInt,
  arinExtractor,
  ripeExtractor
];

// Pick the results of whichever extractor returned the most data
const chooseExtractedData = (data) => data
  .reduce((bestFit, current) => keyCount(bestFit) < keyCount(current)
    ? current
    : bestFit
  );

// Run through all the steps of handling whois data from the raw WHOIS text
const handleWhoisData = (rawData) => flow(
  parseWhois.parseWhoIsData,
  (data) => data.map((pair) => [
    pair.attribute.trim().toLowerCase(),
    pair.value
  ]),
  pairsToObject,
  (data) => extractors.map((e) => e(data, rawData)),
  chooseExtractedData,
  (data) => defaults(data, {
    abuseEmail : [],
    whois : rawData
  })
)(rawData);

const performWhois = (identifier) => promisifiedLookup(identifier)
  .then(handleWhoisData);

export default performWhois;
