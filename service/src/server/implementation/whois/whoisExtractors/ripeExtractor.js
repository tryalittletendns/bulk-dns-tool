const emailRegex = new RegExp('% Abuse contact for \'.*\' is \'(.*)\'');

// The abuse email address is in a comment in the WHOIS response
const extractor = (whoisData, rawData) => {
  const foundEmails = emailRegex.exec(rawData);

  if (!foundEmails || foundEmails.length < 2)
    return {};

  const abuseEmail = foundEmails[1];

  return { abuseEmail };
};

export default extractor;
