const extractor = (whoisData) => ({
  abuseEmail : whoisData['registrar abuse contact email']
});

export default extractor;
