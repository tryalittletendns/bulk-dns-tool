import { lookup } from 'geoip-lite';

export default (ip) => ({
  geoip : lookup(ip)
});
