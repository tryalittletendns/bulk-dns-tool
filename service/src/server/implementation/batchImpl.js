import BatchCollection from '../../db/collection/BatchCollection';
import lookupImpl from './lookupImpl';
import appConfig from '../../config/appConfig';
import _ from 'lodash';

let currentCount = 0;

class BatchService {
    getBatchById(req, res) {
        BatchCollection.findById(req.params.id).then(result => {
            res.status(200).send(result);
        }).catch(error => {
            console.log(error);
            res.status(400).send(error);
        });
    }

    getBatches(req, res) {
        let projection = {results: 0, emailAddresses: 0};
        if (req.query.showResults) {
            projection = {};
        }
        BatchCollection.find(null, projection).then(results => {
            res.status(200).send(results);
        }).catch(error => {
            console.log(error);
            res.status(400).send(error);
        });
    }

    extendBatchExpiryById(req, res) {
        let ttlDate = Date.now() + appConfig.server.mongo.batchLifespanSeconds;
        if (req.query.extendBy) {
            ttlDate = Date.now() + parseInt(req.query.extendBy);
        }
        BatchCollection.updateField(req.params.id, 'ttl', new Date(ttlDate)).then(results => {
            res.status(200).send(results);
        }).catch(error => {
            console.log(error);
            res.status(400).send(error);
        });
    }

    createBatch(req, res) {
        if (!req.body) {
            res.status(400).send('No Body set for incoming create.');
        } else {
            console.log(JSON.stringify(req.body, 0, 4));
            let newBatch = {
                screenName : `Batch ${currentCount++}`,
                searchItems: req.body,
                complete   : false,
                ttl : new Date(Date.now() + 600000)
            };
            BatchCollection.create(newBatch).then(created => {
                res.status(200).send(created);
                lookupImpl.lookupIdentifiers(req.body).then(results => {
                    created.dateComplete = new Date();
                    created.complete = true;
                    created.emailAddresses = [];
                    const dedupeMap = {};
                    for (let result of results) {
                        let existingResult = dedupeMap[result.identifier];

                        if (!existingResult) {
                            existingResult = dedupeMap[result.identifier] = {};
                        }

                        _.merge(existingResult, result);

                        let contact = {
                            domain        : result.identifier,
                            emailAddresses: []
                        };
                        contact.emailAddresses.push(...result.abuseEmail);
                        // TODO other email address locations
                        created.emailAddresses.push(contact);
                    }
                    created.results = Object.values(dedupeMap);
                    BatchCollection.update(created._id, created).then(updated => {
                        console.log('Processing finished and persisted');
                    }).catch(error => {
                        console.log(error);
                    });
                }).catch(error => {
                    console.log(error);
                });
            }).catch(error => {
                console.log(error);
                res.status(400).send(error);
            });
        }
    }
}

export default new BatchService();
