import _ from 'lodash';
import UserCollection from '../../db/collection/UserCollection';
import AuthenticationService from '../../auth/AuthenticationService';
import log from '../../util/log';

const authUserSearchLimit = 10;

class UserImpl {
    getUsers(req, res) {
        UserCollection.find().then(results => {
            res.status(200).send(results);
        }).catch(error => {
            console.log(error);
            res.status(400).send(error);
        });
    }

    getUserById(req, res) {
        UserCollection.findById(req.params.id).then(user => {
            res.status(200).send(this.project(user));
        }).catch(error => {
            console.log(error);
            res.status(400).send(error);
        });
    }

    searchAuthUsers(req, res) {
        const authStrategy = AuthenticationService.getStrategy(),
              limit        = req.query.limit || authUserSearchLimit,
              search       = req.query.search;

        UserCollection.findByTextSearch(search, limit)
            .then(users => {
                if (users.length >= limit) {
                    return res.status(200).send(users.map(this.project).slice(0, limit));
                }

                authStrategy.searchUsers(req.user, search, limit)
                    .then(authUsers => {
                        authUsers.forEach(authUser => {
                            if (!_.find(users, user => user.authId === authUser.authId)) {
                                users.push(authUser);
                            }
                        });

                        sortByRelevance(users, search);

                        res.status(200).send(users.map(this.project).slice(0, limit));
                    })
                    .catch(err => {
                        log.error(err);
                        if (err.status) {
                            return res.status(err.status).send(err);
                        }

                        res.status(500).send(err);
                    });
            });
    }

    project(user) {
        return _.pick(user, ['_id', 'authId', 'name', 'email', 'admin']);
    }
}

export default new UserImpl();

function sortByRelevance(users, search) {
    users.sort((u1, u2) => {
        const score1 = calculateUserScore(u1, search),
              score2 = calculateUserScore(u2, search);

        return score2 - score1;
    });
}

function calculateUserScore(user, search) {
    let score = 0;
    user._id && (score += 4);
    checkStartsWith(user, 'name', search) && (score += 2);
    checkStartsWith(user, 'email', search) && (score += 1);
    return score;
}

function checkStartsWith(user, property, search) {
    const lowerSearch = search ? search.toLowerCase() : '';
    return user && user[property] && user[property].toLowerCase().startsWith(lowerSearch);
}
