var validateIP = require('validate-ip-node');
var dns = require('dns');

const lookup = (identifier) => new Promise((res, rej) => dns.lookup(identifier, (e, d) => {
  if (e)
    return rej(e);

  res(d);
}))
.then((ip) => ({
  ip,
  hostnames : [ identifier ]
}));

const reverseLookup = (identifier) => new Promise((res, rej) => dns.reverse(identifier, (e,d ) => {
  if (e)
    return rej(e);

  res(d);
}))
.then((hostnames) => ({
  ip : identifier,
  hostnames
}));

const dnsLookup = (identifier) => validateIP(identifier)
  ? reverseLookup(identifier)
  : lookup(identifier);

export default dnsLookup;
