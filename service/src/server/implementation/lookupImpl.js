import whois from './whois/whoisImpl';
import dns from './dns/dnsImpl';
import geoip from './geoIp/geoIpImpl';

const mergeWhoisData = (whoisArray) => {
    // Just pick one whois block
    // TODO - Include all of them
    const whois = whoisArray[whoisArray.length - 1].whois;

    const abuseEmail = whoisArray
        .map((whois) => whois.abuseEmail)
        .reduce((combined, array) => combined.concat(array), []);

    return {whois, abuseEmail};
};

class LookupImpl {

    /*
     * Lookup a list of IPs
     */
    lookupIdentifiers(identifierList) {
        return Promise.all(
            identifierList.map((d) => this.enrichIdentifier(d))
    )
    .then((d) => d.filter((i) => !!i));
    }

    // Do all of the enrichments on a single identifier
    enrichIdentifier(identifier) {
        const ipLookup = this.dnsLookup(identifier);

        const whoisLookup = ipLookup
            .then((dnsData) => {
                const whoisPromises = [
                    this.whoisLookup(dnsData.ip)
                ];

                if (dnsData.ip !== identifier)
                    whoisPromises.push(this.whoisLookup(identifier));
                return Promise.all(whoisPromises);
            })
            .then(mergeWhoisData);

        return Promise.all([
                whoisLookup,
                ipLookup
            ])
            .then((data) => Object.assign(
                {identifier},
                this.geoIpLookup(data[1].ip),
                ...data
    ))
    .catch((e) => null);
    }

    // Do the whois lookup
    whoisLookup(identifier) {
        return whois(identifier);
    }

    // Look up either the IP or the hostnames
    dnsLookup(identifier) {
        return dns(identifier);
    }

    // Geo IP lookup
    geoIpLookup(ip) {
        return geoip(ip);
    }

}

export default new LookupImpl();
