import UserImpl from '../implementation/userImpl';

export default function(app, authenticate) {
    app.get('/user', authenticate(), (req, res) => {
        res.status(200).send(UserImpl.project(req.user));
    });

    /*
     * Search for users from the authentication mechanism (e.g. Google, the corporate directory, etc). These users do
     * not need to exist in our database.
     */
    app.get('/users/search', authenticate(), (req, res) => {
        UserImpl.searchAuthUsers(req, res);
    });

    /*
     * Get a User from the database for a given id
     */
    app.get('/users/:userId', authenticate(), (req, res) => {
        UserImpl.getUserById(req, res);
    });

    /*
     * Get all Users from the database
     */
    app.get('/users', authenticate(), (req, res) =>
        UserImpl.getUsers(req, res)
    );
}
