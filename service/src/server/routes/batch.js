import BatchImpl from '../implementation/batchImpl';

/*
 * Main Batch route that describes all the RESTful services available to interact with Batch 
 */
export default function(app, authenticate) {

    /*
     * Get Batch for Id from the database 
     */
    app.get('/batch/:id', authenticate(), (req, res) =>
        BatchImpl.getBatchById(req, res)
    );

    /*
     * Change the batch TTL.
     */
    app.put('/batch/:id/extend', authenticate(), (req, res) =>
        BatchImpl.extendBatchExpiryById(req, res)
    );

    /*
     * Get all base Batches
     */
    app.get('/batches', authenticate(), (req, res) =>
        BatchImpl.getBatches(req, res)
    );

    /*
     * Create new Batch.
     */
    app.post('/batch', authenticate(), (req, res) =>
        BatchImpl.createBatch(req, res)
    );
}
