import _fs from 'fs';
import path from 'path';

import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import Promise from 'bluebird';
import passport from 'passport';

import appConfig from '../config/appConfig';
import log from '../util/log';
import AuthenticationService from '../auth/AuthenticationService';
import User from '../db/model/User';

const fs = Promise.promisifyAll(_fs);
const router = express.Router();

export default class Server {
    constructor(args) {
        this.args = args || {};
        this.app = express();
    }

    configure() {
        const app = this.app;

        this.addMiddlewares(app);

        this.configureAuthentication();

        const routesPath = path.resolve(__dirname, 'routes');
        return fs.accessAsync(routesPath)
            .then(() => fs.readdirAsync(routesPath))
            .then(files => {
                files.forEach(file => {
                    const config = require(`./routes/${file}`).default;
                    config(router, this.authenticateApi);
                });
            })
            .catch(err => {
                if (err && err.code === 'ENOENT') {
                    log.warn('No routes found to configure');
                    return Promise.resolve();
                }
                return Promise.reject(err);
            })
            .then(() => {
                AuthenticationService.configureAuthRoutes(router);
            });
    }

    configureAuthentication() {
        AuthenticationService.configurePassport(User);
    }

    authenticateApi() {
        const strategy = AuthenticationService.getStrategy();
        return strategy.authenticateApi({session: false});
    }

    addMiddlewares(app) {
        app.use(bodyParser.json());
        app.use(cookieParser());
        app.use(cors(generateCorsConfig()));
        app.use(passport.initialize());
        app.use('/api', router);
        app.use(express.static('public'));
        app.use((req, res) => {
            res.sendFile(path.resolve('public/index.html'));
        });
    }

    start() {
        this.configure().then(() => {
            const host = appConfig.server.host,
                  port = appConfig.server.port;
            this.server = this.app.listen(port, host, () => {
                log.info(`Tommy's Totaliser service listening on http://${host}:${port}`);
            });
        });
    }

    stop() {
        if (this.server) {
            this.server.close();
        }
    }
}

function generateCorsConfig() {
    const allowAllOrigins = appConfig.cors.allowAllOrigins,
          whitelist       = appConfig.cors.whitelist;

    return {
        origin     : (origin, cb) => {
            if (allowAllOrigins) {
                log.debug(`[CORS] Allowing origin ${origin}`);
                cb(null, true);
            } else if (whitelist) {
                if (whitelist.indexOf(origin) === -1) {
                    const message = `[CORS] Origin ${origin} is not whitelisted`;
                    log.debug(message);
                    cb(new Error(message));
                } else {
                    log.debug(`[CORS] Allowing origin ${origin} - origin is whitelisted`);
                    cb(null, true);
                }
            } else {
                const message = `[CORS] Origin ${origin} not allowed`;
                log.debug(message);
                cb(new Error(message));
            }
        },
        credentials: true
    };
}
