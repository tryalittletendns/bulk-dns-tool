import yargs from 'yargs';
import _ from 'lodash';
import packageJson from '../../package.json';

import config from '../../config';

const argv = yargs.argv;

const appConfig = {
    dev                    : !!argv.dev,
    version                : packageJson.version,
    authenticationStrategy : 'google',
    autoCreateUser         : true,
    server                 : {
        host: 'localhost',
        port: 3000
    },
    ui                     : {
        host: 'localhost',
        port: 8080
    },
    dataDir                : 'data',
    db                     : {
        host: 'localhost',
        port: 27017,
        name: 'bulk-dns'
    },
    cors                   : {
        allowAllOrigins: true
    },
    authCookieName         : `bulk-dns-tool-${packageJson.version}`,
    authCookieLifetimeHours: 12,
    google                 : {
        scopes         : [
            'profile',
            'email',
            'https://www.googleapis.com/auth/admin.directory.user.readonly'
        ],
        directoryApiUrl: 'https://www.googleapis.com/admin/directory/v1'
    }
};

_.merge(appConfig, config);

export default appConfig;
