import path from 'path';
import mkdirp from 'mkdirp';
import bunyan from 'bunyan';

import appConfig from '../config/appConfig';

const logPath = `${path.resolve(appConfig.logDir)}`;

mkdirp.sync(logPath);

const log = bunyan.createLogger({
    name   : 'totaliser',
    level  : appConfig.dev ? 'debug' : 'info',
    streams: [
        {
            stream: process.stdout
        },
        {
            type  : 'rotating-file',
            path  : `${logPath}/server.log`,
            period: '1d',
            count : 6
        }
    ]
});

if (appConfig.dev) {
    log.info('Application running in development mode');
}

export default log;
