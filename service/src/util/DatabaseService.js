import mongoose from 'mongoose';

import appConfig from '../config/appConfig';
import log from './log';

const connectionString = `mongodb://${appConfig.db.host}:${appConfig.db.port}/${appConfig.db.name}`;

mongoose.Promise = Promise;

class DatabaseService {
    connect() {
        return mongoose.connect(connectionString, {useMongoClient: true})
            .then(() => {
                log.info(`Database connection to ${connectionString} established`);
            });
    }

    disconnect() {
        return mongoose.disconnect()
            .then(() => {
                log.info(`Database connection to ${connectionString} terminated`);
            });
    }
}

export default new DatabaseService();
