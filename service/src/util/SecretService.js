import _fs from 'fs';
import path from 'path';
import _crypto from 'crypto';
import Promise from 'bluebird';

const fs              = Promise.promisifyAll(_fs),
      crypto          = Promise.promisifyAll(_crypto),
      secretsFilePath = path.resolve(__dirname, '../../secrets.json');

class SecretService {
    constructor() {
        this.getSecrets();
    }

    init() {
        return Promise.all([
            this.getJwtSecret()
        ]);
    }

    getSecrets() {
        if (this.secrets) {
            return Promise.resolve(this.secrets);
        }

        try {
            this.secrets = require(secretsFilePath);
            return Promise.resolve(this.secrets);
        } catch (err) {
            this.secrets = {};
            return fs.writeFileAsync(secretsFilePath, JSON.stringify(this.secrets))
                .then(() => this.secrets);
        }
    }

    getJwtSecret() {
        return this.getSecrets()
            .then(secrets => {
                if (secrets.jwtSecret) {
                    return Promise.resolve(Buffer.from(secrets.jwtSecret, 'hex'));
                } else {
                    return crypto.randomBytesAsync(256)
                        .then(buf => {
                            secrets.jwtSecret = buf.toString('hex');
                            return fs.writeFileAsync(secretsFilePath, JSON.stringify(secrets))
                                .then(() => buf)
                                .catch(err => {
                                    console.error(err);
                                })
                        });
                }
            })
    }
}

export default new SecretService();
